+++
date = "2016-06-23T00:00:00"
draft = false
title = "about"
section_id = 0
weight = 0
revised = "2020-11-25"
+++

### Biography

This is the personal website of Harold Hodgins, a freelance data wrangler and research automation consultant.  He can be found wrangling data and doing research  at the [University of Waterloo](https://www.uwaterloo.ca). During his undergraduate degree he could found in the [Biology](https://students.wlu.ca/programs/science/biology/index.html),  [Chemistry](https://students.wlu.ca/programs/science/chemistry-and-biochemistry/index.html), [Physics/Computer Science](https://students.wlu.ca/programs/science/physics-and-computer-science/index.html), and [Psychology](https://students.wlu.ca/programs/science/psychology/index.html) departments at [Wifrid Laurier University](https://www.wlu.ca) or programming liquid handling robots in a cancer research lab.

If you have assays you want automate or hardware you need programmed send me an email and we can chat. 



