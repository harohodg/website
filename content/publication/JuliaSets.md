+++
abstract = "We created programs to create the raw data and pretty pictures of Julia sets in parallel using Python, MPI4PY, and Numpy."
abstract_short = "Poster about creating high resolution images of Julia sets using High Performance Computing"
authors = ["Harold Hodgins"]
date = "2016-05-27T18:39:38-04:00"
image = ""
image_preview = ""
math = false
publication = "diTHINK 2016 conference"
publication_short = ""
title = "Visualizing Julia Sets"
url_code = "https://bitbucket.org/harohodg/julia_sets"
url_dataset = ""
url_pdf = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/JuliaSets_Poster.pdf?viewer=share"
url_project = "project/JuliaSets"
url_slides = ""
url_video = ""

+++

