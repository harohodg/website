+++
abstract = "Poster presented at the International Conference: Applied Mathematics, Modeling and Computational Science Conference in 2015 on our work with Dr. Ian Hamilton on the Spectra of Novel Gold Nanorods. "
abstract_short = "Poster presented at AMMCS 2015"
authors = ["Harold Hodgins"]
date = "2015-06-10"
image = ""
image_preview = ""
math = false
publication = "AMMCS 2015"
publication_short = ""
title = "Optical Spectra of Helical Gold Nanorods: Emergence of the Plasmon Resonance"
url_code = ""
url_dataset = ""
url_pdf = "/publications/AMMCS_2015_poster.pdf"
url_project = ""
url_slides = ""
url_video = ""
+++


