+++
abstract = "We created a program which helps visualize genome annotations stored in a local database."
abstract_short = ""
authors = ["Harold Hodgins"]
date = "2015-07-01"
image = ""
image_preview = ""
math = false
publication = "Directed Studies Final Report"
publication_short = ""
title = "Investigating Homologs using imperfect Human Annotations"
url_code = ""
url_dataset = ""
url_pdf = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/Homologs_and_Imperfect_Annotations.pdf?viewer=share"
url_project = ""
url_slides = ""
url_video = ""
+++


