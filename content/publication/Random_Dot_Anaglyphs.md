+++
abstract = "For our final project in our Computer Graphics course we created a program to create Random Dot Anaglyphs."
abstract_short = "Slides for Random Dot Anaglyphs Presentation"
authors = ["Harold Hodgins"]
date = "2016-12-06"
image = ""
image_preview = ""
math = false
publication = "Computer Graphics Class Presentation"
publication_short = ""
title = "Random Dot Anaglyphs"
url_code = "https://bitbucket.org/harohodg/random-dot-anaglyphs"
url_dataset = ""
url_pdf = ""
url_project = ""
url_slides = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/Random_Dot_Anaglyphs.pdf?viewer=share"
url_video = ""
+++


