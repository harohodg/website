+++
abstract = "We created as Focus Stacking Camera using a Rasberry Pi as part of a directed studies research project."
abstract_short = "Final Report for Directed Studies on Raspberry Pi Focus Stacking Camera"
authors = ["Harold Hodgins"]
date = "2017-04-22"
image = ""
image_preview = ""
math = false
publication = "Directed Studies Final Report"
publication_short = ""
title = "Focus Stacking Camera"
url_code = ""
url_dataset = ""
url_pdf = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/Focus_Stacking_Camera.pdf?viewer=share"
url_project = ""
url_slides = ""
url_video = ""
+++


