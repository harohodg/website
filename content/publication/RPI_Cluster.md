+++
abstract = "We built a small Raspberry Pi Compute Cluster using 10 Raspberry Pi 2.0 computers."
abstract_short = "Final Report and Poster for Raspberry Pi Compute Cluster"
authors = ["Harold Hodgins"]
date = "2015-07-01"
image = ""
image_preview = ""
math = false
publication = "Directed Studies Final Report and Poster"
publication_short = ""
title = "Raspberry Pi Compute Cluster"
url_code = ""
url_dataset = ""
url_pdf = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/RPI_Cluster_Report.pdf?viewer=share"
url_project = ""
url_slides = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/RPI_Cluster_Poster.pdf?viewer=share"
url_video = ""

+++

