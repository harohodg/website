+++
abstract = "In 2017 we worked at the University Health Network in Dr. Trevor Pughs lab as part of the University of Toronto Medial BioPhysics summer program. This is the poster we presented at the end of the summer."
abstract_short = "Poster for MBP Summer Student Poster Day"
authors = ["Harold Hodgins"]
date = "2017-08-10"
image = ""
image_preview = ""
math = false
publication = "MBP Summer Student Poster Day"
publication_short = ""
title = "Automating Laboratory Liquid Handling"
url_code = ""
url_dataset = ""
url_pdf = "https://cocalc.com/share/0a0b3073-8703-4d2f-883d-f4d292cc53c2/Harold_Hodgins_Publications/MBP_Poster.pdf?viewer=share"
url_project = ""
url_slides = ""
url_video = ""
+++


