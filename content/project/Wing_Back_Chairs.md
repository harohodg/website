+++
date = "2017-01-08T15:41:53-05:00"
external_link = ""
image = ""
math = false
summary = "We put two wing back chairs on wheels. "
tags = []
title = "Wing Back Chairs on Wheels"
draft = true
+++

I spend most of my day sitting at a desk or lab bench. In my shared office I have a wing back chair on a mobile platform which my father built for me a few years ago. It's a nice conversation starter when people drop by.

Image of said platform.

Recently I decided to try and build my own version for my electronics lab space since my back gets very unhappy after several hours sitting on the stools in there.

So I bought two wing back chairs at a local auction, and a few 2x4s and got ready to go.

Originally we were planning to replicate the platform from my office which was when we ran into our first problem. The desk in the office is ?? inches off the floor but in the lab the counter
is ?? inches off the floor. Therefore we would need some sort of riser to compensate for the differerence in height. 

After some pondering and dicussing options with a friend we decided to make a box with 4x4 presssure treated (cheapest stuff available) posts, 1x6 pressure treated side panels, and castors on the bottom of the posts.


