+++
date = "2020-01-01T15:24:13-05:00"
external_link = ""
image = ""
math = false
summary = "We built shelves for our kitchen."
tags = ["DIY"]
title = "Kitchen Shelves"

+++

We finally got around to adding more storage capacity to our kitchen by making a simple set of shelves.

<!--more-->

The two major constraints were fitting a kitchen table under the one end and leaving enough room for cupboard doors to open at the other. In the end I went with two shelves. The top one at the same height as the counter with a small gap and a shorter one below. As usual I did most of my work out in the driveway as that's currently our best workspace weather permitting.

The frame we made out of one by two from the local lumber yard held together by roberson screws. I added cross members to make the frames more rigid. 
![Frame](/img/Shelves/frame.jpg)

I considered using up some of my scrap fouton frame wood that I had acquired road side but in the end went with two by fours for the legs. We used blind tee nuts with bolts to attach the legs to the frame and clamps to hold the upper frame on while we did the drilling. It was intended that every thing be symetric so any leg could go in any position but that didn't happen.

My kitchen has baseboard and I wanted the shelves to be as flush to the wall as possible so I added links between the top and bottom frame using scrap fouton wood.
![Frame with bits](/img/Shelves/frame_with_bits.jpg)

For a top we went with hardware cloth attached with roberson screws and washers. It was frustrating to cut it since it wanted to curl up into a cylinder. Was I do redo this project I'd use one by 12 boards. Before adding the hardware cloth we gave everything two coats of clear coat.

![Empty Shelves](/img/Shelves/empty_shelves.jpg)


Overall a success.
![Full Shelves](/img/Shelves/full_shelves.jpg)

That table is now gone. A project for another day is to replace it and perhaps replace the hardware cloth with boards at the same time.
