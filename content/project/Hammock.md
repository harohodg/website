+++
date = "2016-09-25T14:22:33-04:00"
external_link = ""
image = ""
math = false
summary = "We built a self supporting indoor hammock stand."
tags = []
title = "Indoor Hammock Stand"

+++
A few months ago before school started again I decided I wanted to try sleeping in a hammock to see if it would make my back happier. Since my parents had an old hammock gathering dust I just needed to build some sort of supporting frame. Ideally selft suppporting and not attached to the walls making it removable the next time I move. After discussions with my father who used to work in construction we came up with the following design. 

A simple cross piece (a 11ft peice of 2x6 hard wood from the local ReStore) supported by 2x4s screwed together. With 2x4 angled supports and bookshelve counter weights at both ends. Since we also had a wall of book shelves that needed to be discouraged from falling we used a 4x8 sheet of plywood to connect that to the hammock frame. 

{{< figure src="/img/Hammock/far_end.jpg" title="Far end of the frame." >}}
{{< figure src="/img/Hammock/near_end.jpg" title="Near end of the frame." >}}

In the end we bolted the supports to the counter weight bookshelves and the plywood on the one end. And bolted the wall of bookshelves to the plywood as well. The counterweight bookshelves on the near end got attached to the plywood with screws to minimize damage to the books which may eventually be stored there.

{{< figure src="/img/Hammock/bookshelve_bolts.jpg" title="Book shelf bolts." >}}

The hammock loops were fraying so we reinfored them with nylon string and harness rings.

{{< figure src="/img/Hammock/original_end.jpg" title="Original hammock end." >}}
{{< figure src="/img/Hammock/ring_renforced.jpg" title="Reinfored hamock end." >}}


In the end we got the following which so far has been working quite well for sleeping in.

{{< figure src="/img/Hammock/final_result.jpg" title="Final result." >}}


