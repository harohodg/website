+++
date = "2016-06-14T14:14:36-04:00"
draft = true
external_link = ""
image = ""
math = false
summary = "Time Lapses of Clearing and Staining Zebra Fish."
tags = []
title = "Multicolored Pickled Fish"
+++

Diaphonization (also known as clearing and staining) is a process for making specimens transparent with brighly colored bones and cartilage. 

We're currently gearing up to take some time lapses of the process applied to a few zebra fish as a proof of conecept. Assuming it works we plan
to repeat the work with other specimens.
