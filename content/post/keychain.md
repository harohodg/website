+++
date = "2018-05-22T11:23:15-04:00"
image = ""
math = false
tags = ["DIY", "Repair","Keychain"]
title = "Keychain Repair"

+++

My keychain recently broke ties with my keys. As I'd rather not loose my keys I decided to repair the bond between them.

<!--more-->

![Keychain Failure](/img/Keychain/broken.jpg)

To make the hole we used a #8 robertson screw to punch through the material.
![Making Hole](/img/Keychain/hole_making.jpg)


![Hole Success](/img/Keychain/hole_success.jpg)


To attach the ring we used the same screw to pry the ring open enough to thread it through the hole. Note : cheap rings just deform permanently so use a good ring if you do this. If we wanted to reinforce the hole we could have put a gromet through the hole.
![Ring Prying](/img/Keychain/ring_pry1.jpg)

And voila back to not having my keys wandering off spontaneously. 
![Happy Keychain](/img/Keychain/success.jpg)
