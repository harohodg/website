+++
date = "2016-09-06T12:02:48-04:00"
external_link = ""
image = ""
math = false
summary = "We fixed our broken food processor."
tags = []
title = "Fixing Our Broken Food Processor"

+++

I was preparing to wash some dishes the other day when I saw some plastic bits in the bottom of my sink which looked suspiciously like bits of my food processor. Upon further inspection it turned out I was correct and that they were part of the mechanism for telling the machine that the lid was on correctly (it refuses to turn on otherwise). The entire mechanism is spring loaded and of course the spring was no where to be found. It likely took a trip down the drain.

Thankfully I have a small collection of miscellaneous pens including those with springs to I cannibalized one for it's spring. Unfortunately the spring was too good of a fit and once compress on the pin it would't uncompressed.

{{< figure src="/img/foodProcessor/pen_spring.jpg" title="Spring from a random pen." >}}

So I found some random springs at Home Hardware in the locks section and they worked just fine after I chopped them down to length with a set of pliers since I couldn't't find my side cutters. 

{{< figure src="/img/foodProcessor/good_spring.jpg" title="Spring from Home Hardware." >}}

After doing a test fit I cleaned the plastic bit that needed to be glued, scruffed up the adjacent surfaces, glued it together with some Elmers Fix All and weighed it down with what I assume is a jack that I had lying around. 

{{< figure src="/img/foodProcessor/in_place.jpg" title="Test fit" >}}
{{< figure src="/img/foodProcessor/glued.jpg" title="Glued" >}}

I let it sit for almost 48 hours so now we just have to see how long it lasts.
