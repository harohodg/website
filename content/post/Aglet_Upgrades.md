+++
date = "2018-05-02T13:35:32-04:00"
image = ""
math = false
tags = ["Sewing", "Heat Shrink Tubing", "Aglets", "DIY"]
title = "Aglet Upgrades"

+++
"An aglet is a small sheath, often made of plastic or metal, used on each end of a shoelace, a cord, or a drawstring." [Wikipedia](https://en.wikipedia.org/wiki/Aglet) You know, that thing that at the end of your shoelace that disintegrates after a few years and leaves your shoelace exposed and unravelling. Or if you're of a certain age that thing Phineas and Ferb wrote a song about.

<!--more-->

I've had my current shoes for several years and as expected the aglets started giving out. I tried the usual repair method of using scotch tape but that's not a permanent solution.

![Old Bulb](/img/Aglets/bad_tape.jpg)

I thought I'd try using some heat shrink tubing I had in lab. So I removed the tape, cut a small length of tubeing (~2cm), and put it over the end of the shoelace. I had to use thread to tie some of the shoelace ends together so the tubing would fit over. Then I carefully applied heat using a BBQ lighter and voila.

![Binding](/img/Aglets/binding.jpg)
![First Pass Result](/img/Aglets/result.jpg)

This worked decently but I found after a few weeks that some of the tubes were sliding off. So we removed them, put new tubing on, and ran thread through the new tubing and the internal shoelace before applying the heat. And voila encore.

![Failing Tubing](/img/Aglets/bad_tubing.jpg)
![Adding thread to Tubing](/img/Aglets/sewing.jpg)
![Final Result](/img/Aglets/sewn.jpg)


So far this upgrade has been holding well and looks like a reasonable thing to use in future.

### References
* [Wikipedia](https://en.wikipedia.org/wiki/Aglet)

### Aglet Song
* [Phineas and Ferb Song](https://www.youtube.com/watch?v=Evcsj1gx1CE)

