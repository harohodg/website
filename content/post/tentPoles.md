+++
date = "2020-01-01T14:20:30-05:00"
image = ""
math = false
tags = ["DIY", "Repair"]
title = "Tent Poles Repair"

+++
My brothers, my father and I used to go camping every summer up at Tobermory or Killarney. In recent years I've been able to get away a few times but usually on my own. Last summer my eldest brother decided he would like to go camping again near the end of June and then again as August turned to September. He invested in a $30 tent on Kijiji. The previous owners were honest about how much it had been used. In particular the only thing "wrong" with is was the elastic in the poles was shot. After we returned from the first trip I replaced the elastic as shown below.

<!--more-->

![Original Poles](/img/TentPoles/original_poles.jpg)

In our first attempt we attached some elastic thread to a button with some normal thread on the other end and thence attached to a needle thin enough to drop through the holes in the poles. We pulled the elastic thread through and then discovered it was too thin to be useful.

![Elastic Thread](/img/TentPoles/elastic_thread.jpg)
![Button](/img/TentPoles/button.jpg)
![Needle](/img/TentPoles/needle.jpg)

On our second attempt we replaced the elastic thread with more traditional elastic and used a knot instead of a button to keep it from wandering. On the terminal end we pulled the elastic as tight as we could adding about a foot of length beyond the final pole before knotting it and letting it disappear inside.

![Elastic](/img/TentPoles/elastic.jpg)

Overall a success
![Elastic](/img/Camping/tent_1.jpg)

